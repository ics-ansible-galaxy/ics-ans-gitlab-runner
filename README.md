ics-ans-gitlab-runner
=====================

Ansible playbook to install gitlab-runner.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
