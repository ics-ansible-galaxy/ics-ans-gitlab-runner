import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('gitlab_runner')


def test_molecule_envs(host):
    molecule_env = host.file('/opt/conda/envs/molecule/bin/molecule')
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-gitlab-runner-ubuntu22":
        assert molecule_env.exists
    else:
        assert not molecule_env.exists


def test_packer_installed(host):
    cmd = host.run('packer --version')
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-gitlab-runner-ubuntu22":
        assert cmd.rc == 0
    else:
        assert cmd.rc == 127


def test_vagrant_installed(host):
    cmd = host.run('vagrant version')
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-gitlab-runner-ubuntu22":
        assert cmd.rc == 0
    else:
        assert cmd.rc == 127


def test_docker_installed(host):
    docker = host.service('docker')
    assert docker.is_running
    assert docker.is_enabled
